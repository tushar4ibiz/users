package io.tusharnimbokar.userprofile.preesentation.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.tusharnimbokar.userprofile.common.Event
import io.tusharnimbokar.userprofile.data.api.APIService
import io.tusharnimbokar.userprofile.data.models.RandomUser
import io.tusharnimbokar.userprofile.data.models.UserProfile
import io.tusharnimbokar.userprofile.data.repository.SubscriberRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class UserViewModel(private val repository: SubscriberRepository) :ViewModel() {

    var subscriber = repository.subscribers
    private val statusMessage = MutableLiveData<Event<String>>()

    val message : LiveData<Event<String>>
        get() = statusMessage

    fun getUsers(isUpdate:Boolean) {
        runBlocking(CoroutineScope(Dispatchers.IO).coroutineContext) {
            withContext(Dispatchers.IO) {
                getUsersFromAPI(isUpdate)
            }
        }
    }

    fun getUsersFromAPI(isUpdate:Boolean) {
        runBlocking(CoroutineScope(Dispatchers.IO).coroutineContext) {
            withContext(Dispatchers.IO) {
                lateinit var userList: List<UserProfile>
                try {
                    val retrofit = Retrofit.Builder()
                        .baseUrl(APIService.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
                    val api = retrofit.create(APIService::class.java!!)
                    val call = api.getProfilesUser()
                    call.enqueue(object : Callback<RandomUser> {
                        override fun onResponse(
                            call: Call<RandomUser>,
                            response: Response<RandomUser>
                        ) {

                            val body = response.body()
                            if (body != null) {
                                userList = body.userProfile
                                runBlocking(CoroutineScope(Dispatchers.IO).coroutineContext) {
                                    withContext(Dispatchers.IO) {
                                        if (isUpdate){
                                            repository.deleteAll()
                                            repository.insert(userList)
                                        }
                                    }
                                }

                            }

                        }

                        override fun onFailure(call: Call<RandomUser>, t: Throwable) {
                            statusMessage.value = Event(t.message.toString())
                        }
                    })

                } catch (exception: Exception) {
                    statusMessage.value = Event(exception.message.toString())
                }
            }
        }

    }

    fun changeStatus(phone: String, status: String?, isFav:Boolean?){
        runBlocking(CoroutineScope(Dispatchers.IO).coroutineContext) {
          withContext(Dispatchers.IO) { repository.changeStatus(phone,status) }
        }

    }


}