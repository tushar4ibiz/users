package io.tusharnimbokar.userprofile.preesentation.user

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.tusharnimbokar.userprofile.data.repository.SubscriberRepository

class UserViewModelFactory(
    private val repository: SubscriberRepository
):ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UserViewModel(repository) as T
    }
}