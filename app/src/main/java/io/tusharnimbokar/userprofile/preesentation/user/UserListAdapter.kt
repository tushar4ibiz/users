package io.tusharnimbokar.userprofile.preesentation.user

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import io.tusharnimbokar.userprofile.R
import io.tusharnimbokar.userprofile.data.models.UserProfile
import io.tusharnimbokar.userprofile.databinding.ListItemBinding


class UserListAdapter() : RecyclerView.Adapter<MyViewHolder>() {
    private val userList = ArrayList<UserProfile>()
    lateinit var fav: Favourite


    fun setList(artists: List<UserProfile>) {
        userList.clear()
        userList.addAll(artists)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ListItemBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.list_item,
            parent,
            false
        )

        fav = parent.context as Favourite
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(userList[position], fav)

    }

}

public interface Favourite {
    fun changeStatus(id: String, isFav: Boolean, status: String)
}


class MyViewHolder(val binding: ListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(users: UserProfile, fav: Favourite) {
        binding.titleTextView.text = "${users.names.title}.${users.names.first} ${users.names.last}"
        binding.descriptionTextView.text =
            "${users.gender},${users.dobs.age} year \n${users.locations.city}," +
                    "${users.locations.state},${users.locations.country}-" +
                    "${users.locations.postcode}."

        when (users.status) {
            "accept" -> {
                binding.status.visibility = ViewGroup.VISIBLE
                binding.status.text = "Member Accepted"
                binding.status.setTextColor(Color.WHITE)
                binding.llAccpetDecline.visibility = ViewGroup.GONE
            }
            "decline" -> {
                binding.status.visibility = ViewGroup.VISIBLE
                binding.status.text = "Member Declined"
                binding.status.setBackgroundColor(Color.RED)
                binding.status.setTextColor(Color.WHITE)
                binding.llAccpetDecline.visibility = ViewGroup.GONE

            }
            "NA" -> {
                binding.status.visibility = ViewGroup.GONE
                binding.llAccpetDecline.visibility = ViewGroup.VISIBLE
            }
        }

        /**
         * Profile image set.
         */
        Glide.with(binding.imageView.context)
            .load(users.pictures.large)
            .into(binding.imageView)

        /**
         * Accept button click.
         */
        binding.btnAccept.setOnClickListener {
            fav.changeStatus(users.phone, true, "accept")
        }
        /**
         * Decline button click.
         */
        binding.btnDecline.setOnClickListener {
            fav.changeStatus(users.phone, false, "decline")
        }

    }


}