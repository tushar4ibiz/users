package io.tusharnimbokar.userprofile.preesentation.user

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import io.tusharnimbokar.userprofile.R
import io.tusharnimbokar.userprofile.data.db.AppDatabase
import io.tusharnimbokar.userprofile.data.repository.SubscriberRepository
import io.tusharnimbokar.userprofile.databinding.ActivityUserBinding

class UserActivity : AppCompatActivity(),Favourite {
    lateinit var binding: ActivityUserBinding
    private lateinit var userViewModel: UserViewModel
    private lateinit var adapter: UserListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user)

        val dao = AppDatabase.getInstance(applicationContext).randomUserDao()
        val repository = SubscriberRepository(dao)
        val factory = UserViewModelFactory(repository)

        userViewModel = ViewModelProvider(this,factory).get(UserViewModel::class.java)
        binding.lifecycleOwner = this

        userViewModel.message.observe(this, Observer {
            it.getContentIfNotHandled()?.let {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            }
        })

        initRecyclerView()
    }

    private fun initRecyclerView() {
        binding.usersRecyclerView.layoutManager = LinearLayoutManager(this)
        adapter = UserListAdapter()
        binding.usersRecyclerView.adapter = adapter
        displayUserList()
    }
    private fun displayUserList(){
        binding.usersProgressBar.visibility = View.VISIBLE
        val responseLiveData = userViewModel.subscriber
        responseLiveData.observe(this, Observer {
            if(it.size>0){
                adapter.setList(it)
                adapter.notifyDataSetChanged()
                binding.usersProgressBar.visibility = View.GONE
            }else{
                userViewModel.getUsers(false)
                Toast.makeText(applicationContext,"Fetching from server", Toast.LENGTH_LONG).show()
                binding.usersProgressBar.visibility = View.GONE

            }
        })
    }
    override fun changeStatus(id: String, isFav: Boolean, status: String) {
        userViewModel.changeStatus(id,status,isFav)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater : MenuInflater = menuInflater
        inflater.inflate(R.menu.update,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            R.id.action_update -> {
                updateUsers()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    private fun updateUsers() {
        Toast.makeText(applicationContext,"Updating list from server", Toast.LENGTH_LONG).show()
        userViewModel.getUsers(true)
    }

}