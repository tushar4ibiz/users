package io.tusharnimbokar.userprofile.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "random_user")
data class UserProfile (
	@ColumnInfo @SerializedName("gender") val gender : String = "",
	@ColumnInfo @SerializedName("name") val names : Names = Names(),
	@ColumnInfo @SerializedName("location") val locations : Locations = Locations(),
	@ColumnInfo @SerializedName("email") val email : String = "",
	@ColumnInfo @SerializedName("login") val logins : Logins = Logins(),
	@ColumnInfo @SerializedName("dob") val dobs : Dobs = Dobs(),
	@ColumnInfo @SerializedName ("registered") val registered : Registered = Registered(),
	@PrimaryKey
	@ColumnInfo @SerializedName("phone") val phone : String = "",
	@ColumnInfo @SerializedName("cell") val cell : String = "",
	@ColumnInfo @SerializedName("id") val ids : Ids = Ids(),
	@ColumnInfo @SerializedName("picture") val pictures : Pictures = Pictures(),
	@ColumnInfo(name = "isFavourite")var isFavourite :Boolean= false,
	@ColumnInfo(name = "status")var status :String = "NA",
	@ColumnInfo @SerializedName("nat") val nat : String = ""
):Serializable