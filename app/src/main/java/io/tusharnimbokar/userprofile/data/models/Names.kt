package io.tusharnimbokar.userprofile.data.models
import com.google.gson.annotations.SerializedName

data class Names (
	@SerializedName("title") val title : String = "",
	@SerializedName("first") val first : String = "",
	@SerializedName("last") val last : String = ""
)