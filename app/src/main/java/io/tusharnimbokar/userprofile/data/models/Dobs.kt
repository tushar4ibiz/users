package io.tusharnimbokar.userprofile.data.models
import com.google.gson.annotations.SerializedName

data class Dobs (
	@SerializedName("date") val date : String = "",
	@SerializedName("age") val age : Int = 0
)