package io.tusharnimbokar.userprofile.data.api

import io.tusharnimbokar.userprofile.data.models.RandomUser
import retrofit2.Call
import retrofit2.http.GET


interface APIService {

    @GET("api/?results=$result")
    fun getProfilesUser() : Call<RandomUser>

    companion object {
        const val BASE_URL = "https://randomuser.me/"
        const val result = 10
    }
}