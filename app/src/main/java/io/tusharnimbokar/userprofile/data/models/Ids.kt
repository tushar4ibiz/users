package io.tusharnimbokar.userprofile.data.models
import com.google.gson.annotations.SerializedName

data class Ids (
	@SerializedName("name") val name : String = "",
	@SerializedName("value") val value : String = ""
)