package io.tusharnimbokar.userprofile.data.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.tusharnimbokar.userprofile.data.models.*

/**
 * @property - Converter for saving List as Gson and getting back Gson as List in DB
 */
class DataConverter {

    /**
     * Names
     */
    @TypeConverter
    fun fromNames(value: Names): String {
        val gson = Gson()
        val type = object : TypeToken<Names>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toNames(value: String): Names {
        val gson = Gson()
        val type = object : TypeToken<Names>() {}.type
        return gson.fromJson(value, type)
    }


    /**
     * Locations
     */

    @TypeConverter
    fun fromLocations(value: Locations): String {
        val gson = Gson()
        val type = object : TypeToken<Locations>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toLocations(value: String): Locations {
        val gson = Gson()
        val type = object : TypeToken<Locations>() {}.type
        return gson.fromJson(value, type)
    }

    /**
     * Logins
     */

    @TypeConverter
    fun fromLogins(value: Logins): String {
        val gson = Gson()
        val type = object : TypeToken<Logins>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toLogins(value: String): Logins {
        val gson = Gson()
        val type = object : TypeToken<Logins>() {}.type
        return gson.fromJson(value, type)
    }

    /**
     * Dobs
     */

    @TypeConverter
    fun fromDobs(value: Dobs): String {
        val gson = Gson()
        val type = object : TypeToken<Dobs>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toDobs(value: String): Dobs {
        val gson = Gson()
        val type = object : TypeToken<Dobs>() {}.type
        return gson.fromJson(value, type)
    }

    /**
     * Registered
     */

    @TypeConverter
    fun fromRegistered(value: Registered): String {
        val gson = Gson()
        val type = object : TypeToken<Registered>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toRegistered(value: String): Registered {
        val gson = Gson()
        val type = object : TypeToken<Registered>() {}.type
        return gson.fromJson(value, type)
    }


    /**
     * Ids
     */

    @TypeConverter
    fun fromIds(value: Ids): String {
        val gson = Gson()
        val type = object : TypeToken<Ids>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toIds(value: String): Ids {
        val gson = Gson()
        val type = object : TypeToken<Ids>() {}.type
        return gson.fromJson(value, type)
    }


    /**
     * Pictures
     */

    @TypeConverter
    fun fromPictures(value: Pictures): String {
        val gson = Gson()
        val type = object : TypeToken<Pictures>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toPictures(value: String): Pictures {
        val gson = Gson()
        val type = object : TypeToken<Pictures>() {}.type
        return gson.fromJson(value, type)
    }

}