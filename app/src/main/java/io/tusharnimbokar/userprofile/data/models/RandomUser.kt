package io.tusharnimbokar.userprofile.data.models
import com.google.gson.annotations.SerializedName

data class RandomUser (
	@SerializedName("results")
	val userProfile : List<UserProfile>
)