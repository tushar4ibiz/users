package io.tusharnimbokar.userprofile.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.tusharnimbokar.userprofile.data.models.UserProfile

@Dao
interface RandomUserDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveProfiles(profile:List<UserProfile>)


    @Query("DELETE FROM random_user")
    suspend fun deleteAllRandomUsers()

    @Query("UPDATE random_user SET status = :status WHERE phone = :phone")
    fun changeStatus(phone: String, status: String?): Int

    @Query("SELECT * FROM random_user")
     fun getProfiles():LiveData<List<UserProfile>>
}