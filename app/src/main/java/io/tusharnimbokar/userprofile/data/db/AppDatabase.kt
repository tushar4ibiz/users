package io.tusharnimbokar.userprofile.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import io.tusharnimbokar.userprofile.data.models.UserProfile

@Database(entities = [UserProfile::class],version = 1,exportSchema = false)
@TypeConverters(DataConverter::class)
abstract class AppDatabase :RoomDatabase(){
    abstract fun randomUserDao(): RandomUserDAO
    companion object{
        @Volatile
        private var INSTANCE : AppDatabase? = null
        fun getInstance(context: Context):AppDatabase{
            synchronized(this){
                var instance = INSTANCE
                if(instance==null){
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "subscriber_data_database"
                    ).build()
                }
                return instance
            }
        }

    }
}