package io.tusharnimbokar.userprofile.data.repository

import androidx.lifecycle.LiveData
import io.tusharnimbokar.userprofile.data.db.RandomUserDAO
import io.tusharnimbokar.userprofile.data.models.UserProfile

class SubscriberRepository(private val dao : RandomUserDAO) {

    val subscribers: LiveData<List<UserProfile>>
        get() = dao.getProfiles()

    suspend fun insert(userProfile: List<UserProfile>){
       return dao.saveProfiles(userProfile)
    }

    fun changeStatus(phone: String, status: String?) = dao.changeStatus(phone = phone, status = status)

    suspend fun deleteAll() {
        return dao.deleteAllRandomUsers()
    }
}