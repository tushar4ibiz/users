package io.tusharnimbokar.userprofile.data.models
import com.google.gson.annotations.SerializedName



data class Pictures (
	@SerializedName("large") val large : String = "",
	@SerializedName("medium") val medium : String = "",
	@SerializedName("thumbnail") val thumbnail : String = ""
)